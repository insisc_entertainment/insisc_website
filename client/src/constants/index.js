import { instagram, miami_bici, mirciulica, teambuilding, dorna, luca, webs9, netflix, rci, facebook } from "../assets";

export const sponsors = [
  {
    id: "sponsor-1",
    logo: dorna,
  },
  {
    id: "sponsor-2",
    logo: luca,
  },
  {
    id: "sponsor-3",
    logo: webs9,
  },
  {
    id: "sponsor-4",
    logo: netflix,
  },
  {
    id: "sponsor-5",
    logo: rci,
  },
];

export const movies = [
  {
    id: 1,
    title: "Miami Bici (2020)",
    date: "4 Nov. 2022 | 18:30",
    img: miami_bici,
    trailerURL: "https://www.youtube.com/watch?v=zUOneia4y6A",
  },
  {
    id: 2,
    title: "Mirciulică (2022)",
    date: "5 Nov. 2022 | 18:30",
    img: mirciulica,
    trailerURL: "https://www.youtube.com/watch?v=mpi0qsp3v_w",
  },
  {
    id: 3,
    title: "Teambuilding (2022)",
    date: "6 Nov. 2022 | 19:00",
    img: teambuilding,
    trailerURL: "https://www.youtube.com/watch?v=8bc966ORJn8",
  },
];

export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "movies",
    title: "Filme",
  },
  {
    id: "sponsors",
    title: "Sponsori",
  },
  {
    id: "register",
    title: "Înregistrare",
  },
];

export const features = [
  {
    id: "feature-1",
    icon: "TicketIcon",
    title: "Intrare gratuită",
    content:
      "Dacă ești dornic să vezi filme de calitate alături de oameni de calitate, atunci trebuie neapărat să participi la inSiSC Entertainment!",
  },
  {
    id: "feature-2",
    icon: "GiftIcon",
    title: "Te așteaptă o mulțime de surprize",
    content:
      "După vizionarea filmelor, sponsorii evenimentului ne vor prezenta niște surprize.",
  },
  {
    id: "feature-3",
    icon: "MapPinIcon",
    title: "Casa de Cultură a Studenților București",
    content:
      "Este locația unde se va desfășura inSiSC Entertainment. Te așteptăm cu vibe-uri excelente!",
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/insisc_entertainment/",
  },
  {
    id: "social-media-2",
    icon: facebook,
    link: "https://www.facebook.com/profile.php?id=100086963485213",
  },
];
