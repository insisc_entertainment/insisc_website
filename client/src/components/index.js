import Navbar from "./Navbar";
import Footer from "./Footer";
import Hero from "./Hero";
import Movies from "./Movies";
import Sponsors from "./Sponsors";
import Register from "./Register";
import Features from "./Features";
import SpecialGuest from "./SpecialGuest";

export {
  Navbar,
  Footer,
  Hero,
  Movies,
  Sponsors,
  Register,
  Features,
  SpecialGuest,
};