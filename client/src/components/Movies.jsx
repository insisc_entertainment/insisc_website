import { movies } from '../constants'
import * as Icons from '@heroicons/react/24/outline'
import styles, { layout } from '../style'

const MovieCard = ({img, title, date, trailerURL, index}) => (
  <div className={`group p-4 rounded-[20px] flex flex-col items-center ${index !== movies.length - 1 ? 'mr-6' : 'mr-0'}`}>
    <div className={`w-[150px] h-[200px] ${styles.flexCenter}`}>
      <img src={img} alt="movie_poster" className="ease-in-out duration-300 group-hover:contrast-75 w-[100%] h-[100%] object-contain"/>
      <a href={trailerURL} target="_blank" className='absolute opacity-0 ease-in-out duration-300 cursor-pointer group-hover:opacity-100'><Icons.PlayCircleIcon className='opacity-100 h-10 text-white' /></a>
    </div>
    <div className='flex-1 flex flex-col mt-4'>
      <h4 className='mb-1 font-montserrat font-semibold text-white text-[18px] leading-[23px]'>{title}</h4>
      <p className='mb-1 font-inter font-normal text-dimWhite text-[16px] leading-[24px]'>{date}</p>
    </div>
  </div>
)

const Movies = () => (
  <section id="movies" className={layout.section}>
    <div className={layout.sectionInfo}>
      <h2 className={styles.heading2} >Despre noi</h2>
      <p className={`${styles.paragraph} max-w-[470px] mt-5`}>Primul eveniment cu și despre filme conceput de către studenții CSIE, dornici să promoveze cultura prin intermediul filmului.</p>
    </div>

    <div className={`${layout.sectionImg} flex-row overflow-auto`}>
      {movies.map((movie, index) => (
          <MovieCard key={movie.id} {...movie} index={index} />
      ))}
    </div>
  </section>
)

export default Movies