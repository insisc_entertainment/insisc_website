import styles from '../style'
import { arrowUp } from '../assets'

//Circle button from Hero
const GetStarted = () => (
  <a href="#register">
    <div className={`${styles.flexCenter} w-[140px] h-[140px] rounded-full bg-gradient p-[2px] cursor-pointer`}>
      <div className={`${styles.flexCenter} flex-col bg-primary w-[100%] h-[100%] rounded-full`}>
        <div className={`${styles.flexStart} flex-row`}>
          <p className='font-montserrat font-medium text-[18px] leading-[23px] mr-[2px]'>
            <span className='text-gradient'>Mă</span>
          </p>
          <img src={arrowUp} className="w-[23px] h-[23px] object-contain" alt="arrow" />
        </div>
          <p className='font-montserrat font-medium text-[18px] leading-[23px]'>
            <span className='text-gradient'>Înscriu</span>
          </p>
      </div>
    </div>
  </a>
)


export default GetStarted