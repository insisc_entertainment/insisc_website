import { matei_dima } from '../assets'
import styles, { layout } from '../style'

const SpecialGuest = () => (
    <section id="product" className={layout.sectionReverse}>
      <div className={layout.sectionImgReverse}>
        <img src={matei_dima} alt="matei_dima" className='w-[100%] h-[100%] relative z-[5]'/>
        <div className='absolute z-[3] -left-1/2 top-0 w-[50%] h-[50%] rounded-full white__gradient'/>
        <div className='absolute z-[0] -left-1/2 bottom-0 w-[50%] h-[50%] rounded-full pink__gradient'/>
      </div>

      <div className={layout.sectionInfo}>
        <h2 className={styles.heading2}>Invitat special: <br className="sm:block hidden" /> Matei Dima</h2>
        <p className={`${styles.paragraph} max-w-[470px] mt-5`}>Matei Dima alias BRomania va fi prezent pe 6 noiembrie și ne va povesti despre cariera sa în domeniul cinematografic.</p>
      </div>
    </section>
)


export default SpecialGuest