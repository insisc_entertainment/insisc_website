import { features } from '../constants'
import { movie_bg } from '../assets'
import * as Icons from '@heroicons/react/24/outline'
import styles, { layout } from '../style'

const FeatureCard = ({icon, title, content, index}) => {
  const IconComponent = Icons[icon];
  return (
  <div className={`flex flex-row p-6 rounded-[20px] ${index !== features.length - 1 ? 'mb-6' : 'mb-0'} feature-card`}>
    <div className={`w-[64px] h-[64px] rounded-full ${styles.flexCenter} bg-dimBlue`}>
      <IconComponent className="h-10 w-10 text-orange" />
    </div>
    <div className='flex-1 flex flex-col ml-3'>
      <h4 className='mb-1 font-montserrat font-semibold text-white text-[18px] leading-[23px]'>{title}</h4>
      <p className='mb-1 font-montserrat font-normal text-dimWhite text-[16px] leading-[24px]'>{content}</p>
    </div>
  </div>
)}

const Features = () => (
  <section id="features" className={layout.section}>
    <div className={`flex-1 flex flex-col`}>
      {features.map((feature, index) => (
          <FeatureCard key={feature.id} {...feature} index={index} />
      ))}
    </div>

    <div className={`${layout.sectionInfo}`}>
      <img src={movie_bg} alt="icon" className="sm:ml-4 w-[100%] h-[100%] rounded-[16px] object-cover"/> 
    </div>
  </section>
)

export default Features