import { movies } from '../constants'
import styles, { layout } from '../style'
import { useState } from 'react'
import axios from 'axios';
import Swal from 'sweetalert2';

const { data } = await axios.get('http://localhost:8000/api/v1/movies/available-seats');

const Register = () => {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [selectedMovies, setSelectedMovies] = useState([]);  

  const submitData = async (e) => {
    try {
        e.preventDefault();
        const clientData = {
            firstName, lastName, phone, email, selectedMovies
        };
        if(!selectedMovies.length) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Trebuie sa selectezi macar un film',
                showConfirmButton: false,
                width: '24em',
                timer: 1500
            });
            return;
        }
        const { data } = await axios.post(`http://localhost:8000/api/v1/movies/register`, { clientData });
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: data.message,
            showConfirmButton: false,
            width: '24em',
            timer: 1500
        });
    } catch (error) {
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: error.response.data.message || 'Eroare de pe server :(',
            showConfirmButton: false,
            width: '24em',
            timer: 1500
        });
    }
  }

  function getAvailabilityColor(locuri_disponibile) {
    if(locuri_disponibile > 100)
        return 'green';
    else if(locuri_disponibile > 50)
        return 'orange';
    else return 'red';
  }

  return (
    <section id="register" className={layout.section}>
        <div className={`${layout.sectionInfo}`}>
            <h2 className={`sm:pr-20 ${styles.heading2}`} >Utilizează formularul pentru a-ți înregistra participarea la Eveniment.</h2>
            <p className={`${styles.paragraph} max-w-[470px] mt-5`}>PS: Locurile sunt limitate!</p>
        </div>
        <div className='flex-col flex-1'>
            <form onSubmit={submitData}>
                <div className="grid gap-6 mb-6 md:grid-cols-2">
                    <div>
                        <label htmlFor="first_name" className="block mb-2 text-sm font-montserrat font-medium text-gray-900 dark:text-gray-300">Nume</label>
                        <input type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)} id="first_name" className="font-inter bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="" />
                    </div>
                    <div>
                        <label htmlFor="last_name" className="block mb-2 text-sm font-montserrat font-medium text-gray-900 dark:text-gray-300">Prenume</label>
                        <input type="text" value={lastName} onChange={(e) => setLastName(e.target.value)} id="last_name" className="font-inter bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="" />
                    </div>
                    <div>
                        <label htmlFor="phone" className="block mb-2 text-sm font-montserrat font-medium text-gray-900 dark:text-gray-300">Telefon</label>
                        <input type="tel" value={phone} onChange={(e) => setPhone(e.target.value)}  id="phone" className="font-inter bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" pattern="07[0-9]{8}" required="" />
                    </div>
                    <div className="mb-6">
                        <label htmlFor="email" className="block mb-2 text-sm font-montserrat first-line:font-medium text-gray-900 dark:text-gray-300">Email</label>
                        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} id="email" className="font-inter bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="" />
                    </div> 
                </div>
                <div className="mb-6">
                    <h3 className="font-montserrat mb-5 text-lg font-medium text-gray-900 dark:text-white">La ce filme dorești să vii?</h3>
                    <ul className={`grid gap-6 w-full md:grid-cols-3`}>
                        {movies.map((movie, index) => (
                            <li key={index} className={`${data.movieData[index].locuri_disponibile <= 0 ? 'disabled' : ''}`}>
                                <input type="checkbox" disabled={data.movieData[index].locuri_disponibile <= 0} onChange={(e) => e.target.checked ? setSelectedMovies([...selectedMovies, movie]) : setSelectedMovies(selectedMovies.filter((selectedMovie) => JSON.stringify(selectedMovie) !== JSON.stringify(movie)))} id={movie.title} value={movie.title} className="hidden peer" required="" />
                                <label htmlFor={movie.title} className="inline-flex ss:justify-around justify-between items-center p-2 w-full bg-white rounded-lg border-2 border-gray-200 cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-orange hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">                           
                                    <div className={`w-[50%] h-[150px] ${styles.flexCenter} mr-2`}>    
                                        <img src={movie.img} alt="movie_poster" className="w-[100%] h-[100%] object-contain"/>

                                    </div>
                                    <div className="block w-[50%]">
                                        <div className="sm:w-[100%] w-[140px] font-montserrat ss:text-sm font-lg font-semibold leading-5">{movie.title}</div>
                                        <div className="font-inter text-[10px]">{movie.date}</div>
                                        <p className='text-sm leading-[20px] mt-3'><span className={`bg-${getAvailabilityColor(data.movieData[index].locuri_disponibile)} text-white px-1 rounded-lg font-medium`}>{data.movieData[index].locuri_disponibile}</span> locuri disponibile</p>
                                    </div>
                                </label>
                            </li>
                        ))}
                    </ul>
                </div>
                <button type="submit" className="text-white float-right bg-gradient focus:ring-4 focus:outline-none focus:ring-purple font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:hover:bg-purple dark:focus:ring-purple">Mă înscriu</button>
            </form>
        </div>
    </section>
  )
}

export default Register