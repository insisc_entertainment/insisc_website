import React from 'react'
import styles from '../style'
import { HeartIcon } from '@heroicons/react/24/solid'
import { clapperboard } from '../assets'
import GetStarted from './GetStarted'

const Hero = () => (
  <section id="home" className={`flex md:flex-row flex-col ${styles.paddingY}`}>
    <div className={`flex-1 ${styles.flexStart} flex-col xl:px-0 sm:px-16 px-6`}>
      <div className="flex flex-row items-center py-[6px] px-4 bg-discount-gradient rounded-[10px] mb-2">
        <HeartIcon className="w-[32px] h-[32px] text-purple" />
        <p className={`${styles.paragraph} ml-2`}>
          <span>Intrarea este gratuită pentru toată lumea!</span>
        </p>
      </div>

      <div className="flex flex-row justify-between items-center w-full">
        <h1 className="flex-1 font-montserrat font-semibold ss:text-[72px] text-[52px] text-white ss:leading-[75px] leading-[50px]">
          inSiSC <br/> {" "}
          <span className="text-gradient">Să te uiți!</span> {" "}
        </h1>
        <div className="ss:flex hidden md:mr-4 mr-0">
          <GetStarted />
        </div>
      </div>

      <p className={`${styles.paragraph} max-w-[470px] mt-5`}>Vino alături de noi începând de vineri, 4 noiembrie, pentru a viziona cele mai emblematice filme chiar la Casa de Cultură a Studenților din București! <br/> PS: Popcornul îl vom aduce noi. 😋</p>
    </div>

    <div className={`flex-1 flex ${styles.flexCenter} md:my-0 my-10 relative`}>
      <img src={clapperboard} alt="billing" className="w-[100%] h-[100%] relative z-[5] floating-animation"/>
      <div className="absolute z-[0] w-[40%] h-[35%] top-0 pink__gradient" />
      <div className="absolute z-[1] w-[80%] h-[80%] rounded-full bottom-40 white__gradient" />
      <div className="absolute z-[0] w-[50%] h-[50%] right-20 bottom-20 blue__gradient" />
    </div>
    
    <div className={`ss:hidden ${styles.flexCenter}`}>
      <GetStarted />
    </div>
  </section>
)


export default Hero