import React from 'react'
import styles from './style'

import { Navbar, Hero, Footer, Movies, Sponsors, Register, Features, SpecialGuest } from './components'

const App = () => (
  <div className="bg-primary w-full overflow-hidden">
    <div className={`${styles.paddingX} ${styles.flexCenter}`}>
      <div className={`${styles.boxWidth}`}>
        <Navbar />
      </div>
    </div>

    <div className={`br-primary ${styles.flexStart}`}>
      <div className={`${styles.boxWidth}`}>
        <Hero />
      </div>
    </div>

    <div className={`br-primary ${styles.paddingX} ${styles.flexStart}`}>
      <div className={`${styles.boxWidth}`}>
        <Movies />
        <SpecialGuest />
        <Features />
        <Sponsors /> 
        <Register />
        <Footer />
      </div>
    </div>

  </div>
)


export default App