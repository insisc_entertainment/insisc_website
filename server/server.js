require('dotenv').config();
const express = require('express');
const app = express();
const PORT = process.env.PORT || 8000;
const helmet = require('helmet');
const cors = require('cors');

app.use(helmet());
app.use(express.json());
app.use(cors());

// Routes
const routes = require('./routes/index');

app.use('/api/v1', routes);

app.use('*', (req, res) => {
	res.status(404).json({
		message: 'Route not found',
	});
});
app.listen(PORT, () => {
	console.log(`Listening on http://localhost:${PORT}`);
});
