const router = require('express').Router();
const db = require('../db/');

router.post('/movies/register', async (req, res) => {
	try {
		const { clientData } = req.body;
		const emptyField = Object.entries(clientData).some(
			([_, value]) => value === ''
		);
		if (emptyField) {
			res.status(400).json({
				message: 'Camp necompletat',
			});
			return;
		}
		const query = new Promise((resolve, reject) => {
			return db.query(
				'SELECT email FROM registru_filme WHERE email = ?',
				[clientData.email],
				(err, result) => {
					if (err) reject(err);
					resolve(result);
				}
			);
		});

		const existingEmail = await query;
		if (existingEmail.length !== 0) {
			res.status(400).json({
				message: 'Email deja inregistrat',
			});
		} else {
			db.query(
				`INSERT INTO registru_filme (nume, prenume, telefon, email, filme_selectate) values (?, ?, ?, ?, ?);`,
				[
					clientData.lastName,
					clientData.firstName,
					clientData.phone,
					clientData.email,
					JSON.stringify(clientData.selectedMovies),
				],
				(err, result) => {
					if (err) throw err;
					clientData.selectedMovies.forEach((selectedMovie) => {
						db.query(
							'UPDATE locuri_filme SET locuri_disponibile = locuri_disponibile - 1 WHERE nume_film = ? AND locuri_disponibile > 0',
							[selectedMovie.title],
							(err, _) => {
								if (err) throw err;
							}
						);
					});
					res.json({
						message: 'Datele adaugate cu succes',
					});
					return;
				}
			);
		}
	} catch (error) {
		res.status(error.statusCode || 500).json({
			message: error.message || 'Eroare interna de pe server :(',
		});
	}
});

router.get('/movies/available-seats', async (req, res) => {
	try {
		db.query('SELECT * FROM locuri_filme', (err, result) => {
			if (err) throw err;
			res.json({
				movieData: JSON.parse(JSON.stringify(result)),
			});
			return;
		});
	} catch (error) {
		res.status(error.statusCode || 500).json({
			message: error.message || 'Eroare interna de pe server :(',
		});
	}
});

module.exports = router;
