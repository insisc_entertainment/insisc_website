const mysql = require('mysql');
const connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'insisc',
});

connection.connect((err, result) => {
	if (err) throw err;
	connection.query(
		'CREATE TABLE IF NOT EXISTS registru_filme (id INT AUTO_INCREMENT PRIMARY KEY, nume VARCHAR(255) NOT NULL, prenume VARCHAR(255) NOT NULL, telefon VARCHAR(10) NOT NULL, email VARCHAR(255) NOT NULL UNIQUE, filme_selectate JSON NOT NULL);',
		(err, result) => {
			if (err) throw err;
		}
	);
	connection.query(
		'CREATE TABLE IF NOT EXISTS locuri_filme (id INT AUTO_INCREMENT PRIMARY KEY, nume_film VARCHAR(255) NOT NULL, locuri_disponibile INT NOT NULL);',
		(err, result) => {
			if (err) throw err;
		}
	);
	console.log('Connected to DB');
	connection.query('SELECT COUNT(*) FROM locuri_filme', (err, result) => {
		if (err) throw err;
		if (JSON.parse(JSON.stringify(result))[0]['COUNT(*)'] === 3) {
			return;
		} else {
			connection.query(
				`INSERT INTO locuri_filme (nume_film, locuri_disponibile) VALUES ('Miami Bici (2020)', 120);`,
				(err, _) => {
					if (err) throw err;
				}
			);
			connection.query(
				`INSERT INTO locuri_filme (nume_film, locuri_disponibile) VALUES ('Mirciulică (2022)', 80);`,
				(err, _) => {
					if (err) throw err;
				}
			);
			connection.query(
				`INSERT INTO locuri_filme (nume_film, locuri_disponibile) VALUES ('Teambuilding (2022)', 140);`,
				(err, _) => {
					if (err) throw err;
				}
			);
		}
	});
});

module.exports = connection;
